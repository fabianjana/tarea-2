import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *


def drawLine(x, y, X, Y, tx, ty):

    glBegin(GL_LINES)

    glColor3f(0.8, 0, 0)
    glVertex2f(-1.0 + (2*x - 1)*2.0/(2*tx), -1.0 + (2*y - 1)*2.0/(2*ty))

    glColor3f(0.8, 0, 0)
    glVertex2f(-1.0 + (2*X - 1)*2.0/(2*tx), -1.0 + (2*Y - 1)*2.0/(2*ty))

    glEnd()

def paintQuad(x, y, tx, ty, C1 = 1.0, C2 = 1.0, C3 = 1.0):
    
    glBegin(GL_QUADS)
    
    glColor3f(C1, C2, C3)
    glVertex2f(-1.0 + x*(2.0)/(tx), -1.0 + y*(2.0)/(ty))

    glColor3f(C1, C2, C3)
    glVertex2f(-1.0 + (x-1)*(2.0)/(tx), -1.0 + y*(2.0)/(ty))

    glColor3f(C1, C2, C3)
    glVertex2f(-1.0 + (x-1)*(2.0)/(tx), -1.0 + (y-1)*(2.0)/(ty))

    glColor3f(C1, C2, C3)
    glVertex2f(-1.0 + x*(2.0)/(tx), -1.0 + (y-1)*(2.0)/(ty))

    glEnd()

def drawGrid(x, y):

    for i in range(1, x):
        glBegin(GL_LINES)
        glVertex2f(-1.0 + i*(2.0)/(x),-1.0)
        glVertex2f(-1.0 + i*(2.0)/(x),1.0)
        glEnd()

    for i in range(1, y):
        glBegin(GL_LINES)
        glVertex2f(-1.0, -1.0 + i*(2.0)/(y))
        glVertex2f(1.0, -1.0 + i*(2.0)/(y))
        glEnd()

def bresenhamLow(xi, yi, xf, yf, tx, ty, t):
    
    dy = yf - yi
    dx = xf - xi
    y0 = 1
    if dy < 0:
        y0 = -1
        dy = -dy
    p = 2*dy - dx
    x = xi
    y = yi
    T = 0
    while x <= xf and T < t:
        paintQuad(x, y, tx, ty)
        if p < 0:
            p = p + 2*dy
        else:
            y = y + y0
            p = p + 2*dy - 2*dx
        x = x + 1
        T = T + 1


def bresenhamHigh(xi, yi, xf, yf, tx, ty, t):
    
    dy = yf - yi
    dx = xf - xi
    x0 = 1
    if dx < 0:
        x0 = -1
        dx = -dx
    p = 2*dx - dy
    y = yi
    x = xi
    T = 0
    while y <= yf and T < t:
        paintQuad(x, y, tx, ty)
        if p < 0:
            p = p + 2*dx
        else:
            x = x + x0
            p = p + 2*dx - 2*dy
        y = y + 1
        T = T + 1


def Bresenham(xi, yi, xf, yf, tx, ty, t):

    if abs(yf - yi) < abs(xf - xi):
        if xi > xf:
            bresenhamLow(xf, yf, xi, yi, tx, ty, t)
        else:
            bresenhamLow(xi, yi, xf, yf, tx, ty, t)
    else:
        if yi > yf:
            bresenhamHigh(xf, yf, xi, yi, tx, ty, t)
        else:
            bresenhamHigh(xi, yi, xf, yf, tx, ty, t)




def main(tx, ty, xi, yi, xf, yf):

    if tx > ty:
        W = 800
        H = (W / tx)*ty
    else:
        H = 600
        W = (H / ty)*tx

    pygame.init()
    pygame.display.set_mode((int(W), int(H)), OPENGL | DOUBLEBUF)
    pygame.display.set_caption("Bresenham")

    run = True
    t = 0
    while run:

        for event in pygame.event.get():
            if event.type == QUIT:  # close window
                run = False

            if event.type == KEYDOWN:
                if event.key == K_SPACE:
                    pass
                
            if t > 0:
                if event.type == KEYDOWN:
                    if event.key == K_LEFT:
                        t -= 1
                    
            if event.type == KEYDOWN:
                if event.key == K_RIGHT:
                    t += 1 
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)  # clean buffers
        paintQuad(xi, yi, tx, ty, 0.5, 0.5, 0.5)
        paintQuad(xf, yf, tx, ty, 0.5, 0.5, 0.5)
        drawGrid(tx, ty)
        if t > 0:
            Bresenham(xi, yi, xf, yf, tx, ty, t)
        drawLine(xi, yi, xf, yf, tx ,ty)
        pygame.display.flip()  # refresh screen
        pygame.time.wait(int(1000 / 30))  # 30 fps


    pygame.quit()
        

import sys
if __name__ == "__main__":
    arg = sys.argv
    dim = arg[1].split('x')
    fp = arg[2].split(',')
    lp = arg[3].split(',')
    main(int(dim[0]), int(dim[1]), int(fp[0]), int(fp[1]), int(lp[0]), int(lp[1]))
    
